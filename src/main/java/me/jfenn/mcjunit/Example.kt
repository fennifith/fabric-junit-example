package me.jfenn.mcjunit

import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.text.Text

class Example {
    fun getExample(): Text {
        val a = ServerPlayerEntity::class
        val text = Text.literal("Example").append(a.simpleName)
        return text
    }
}